# raii-map

Map and set types in Rust in which the presence of the entry in the data structure is tied to the lifetime of a RAII handle returned by the insertion.