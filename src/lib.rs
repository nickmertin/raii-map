//! Map and set types in Rust in which the presence of the entry in the data
//! structure is tied to the lifetime of a RAII handle returned by the insertion.

#![no_std]
#![warn(missing_docs)]

extern crate alloc;

pub mod map;
pub mod set;
